# Package

version       = "0.1.0"
author        = "refaQtor"
description   = "compile time insertion of git HEAD hash into const string for use at runtime"
license       = "MPL2"
srcDir        = "src"


# Dependencies

requires "nim >= 0.19.4"
